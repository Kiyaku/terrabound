using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Generator : MonoBehaviour {
    public class Coords {
        public int wX;
        public int wY;
        public int cX;
        public int cY;

        public Coords() { }
        public Coords(int wX, int wY, int x, int y) {
            this.wX = wX;
            this.wY = wY;
            this.cX = x;
            this.cY = y;
        }
    }

    public static Generator ins;

    public Transform ChunkPrefab;
    public Texture2D[] Tileset;
    public Material DefaultMat;
    public Color defaultBlockColor = new Color(0.2f, 0.2f, 0.2f);

    private Chunk[] worldArray;
    private byte blockID = 1;
    private List<ChunkMB> chunkMBList = new List<ChunkMB>();

    private List<int> tileIDMap = new List<int> { 0, 2, 8, 10, 11, 16, 18, 22, 24, 26, 27, 30, 31, 64, 66, 72, 74, 75, 80, 82, 86, 88, 90, 91, 94, 95, 104, 106, 107, 120, 122, 123, 126, 127, 208, 210, 214, 216, 218, 219, 222, 223, 248, 250, 251, 254, 255, 256 };


    void Awake() {
        ins = this;

        Generate();
    }


    public int GetWorldIndex(int x, int y) {
        return y * WorldSettings.WorldSizeX + x;
    }


    public int GetBlockIndex(int x, int y) {
        return y * WorldSettings.ChunkSizeX + x;
    }


    public void GenerateEmptyWorld(int worldSizeX = 0, int worldSizeY = 0) {
        if(worldSizeX > 0 && worldSizeY > 0) {
            WorldSettings.WorldSizeX = worldSizeX;
            WorldSettings.WorldSizeY = worldSizeY;
        }

        worldArray = new Chunk[WorldSettings.WorldSizeX * WorldSettings.WorldSizeY];

        for(int i = 0; i < worldArray.Length; i++) {
            int x = i % worldSizeX;
            int y = i / worldSizeX;

            worldArray[i] = new Chunk();
            worldArray[i].ChunkPosition = new Vector3(x * WorldSettings.ChunkSizeX, y * WorldSettings.ChunkSizeY, 0);

            //Transform tempChunk = Instantiate(ChunkPrefab) as Transform;
            //ChunkMB chunkmb = tempChunk.GetComponent<ChunkMB>();
            //chunkmb.Setup(worldArray[i], DefaultMat);
            //chunkMBList.Add(chunkmb);
            //tempChunk.transform.parent = transform;

            //generateMesh(worldArray[i]);
        }
    }


    public void Generate() {
        GenerateEmptyWorld(500, 250);

        int chunkID = 0;

        float mapWidth = WorldSettings.WorldSizeX * WorldSettings.ChunkSizeX;
        float mapHeight = WorldSettings.WorldSizeY * WorldSettings.ChunkSizeY;
        mapWidth *= 0.1f;
        mapHeight *= 0.1f;

        foreach(Chunk chunk in worldArray) {
            float mapX = chunkID % WorldSettings.WorldSizeX;
            float mapY = chunkID / WorldSettings.WorldSizeX;

            for(int b = 0; b < chunk.BlockArray.Length; b++) {
                float bX = b % WorldSettings.ChunkSizeX;
                float bY = b / WorldSettings.ChunkSizeX;
                bX += mapX * WorldSettings.ChunkSizeX;
                bY += mapY * WorldSettings.ChunkSizeY;
                bX /= mapWidth;
                bY /= mapHeight;

                float noise = Mathf.PerlinNoise(bX, bY);
                float height = 60 + noise * 30f;

                if(b / WorldSettings.ChunkSizeX + mapY * WorldSettings.ChunkSizeY <= height) {
                    chunk.BlockArray[b] = (byte)(noise >= 0.3f ? 1 : 2);
                } else {
                    chunk.BlockArray[b] = 0;
                }
            }

            chunkID++;

            //generateMesh(chunk);
        }


        // Calculate light
        fillSunlight();


       // foreach(Chunk chunk in worldArray) {
            //generateMesh(chunk);
       // }

        Debug.Log("Done");
    }


    //   public void Generate(string data) {
    //	GenerateEmptyWorld();

    //	string[] blockString = data.Substring(data.IndexOf("___")).Split('\n');

    //	// Set block types
    //	foreach(string str in blockString) {
    //		string[] dataStr = str.Split(':');

    //		if(dataStr.Length == 5)
    //			worldArray[int.Parse(dataStr[0]), int.Parse(dataStr[1])].BlockArray[int.Parse(dataStr[2]), int.Parse(dataStr[3])] = byte.Parse(dataStr[4]);
    //	}

    //	// Calculate light
    //	fillSunlight();

    //	// Render blocks
    //	for(int x = 0; x < worldArray.GetUpperBound(0); x++) {
    //		for(int y = 0; y < worldArray.GetUpperBound(1); y++) {
    //			generateMesh(worldArray[x, y]);
    //		}
    //	}
    //}


    //public string GetSavedWorld() {
    //	string worldstring = "";

    //	for(int x = 0; x < worldArray.GetUpperBound(0); x++) {
    //		for(int y = 0; y < worldArray.GetUpperBound(1); y++) {
    //			for(int i = 0; i <= worldArray[x, y].BlockArray.GetUpperBound(0); i++) {
    //				for(int j = 0; j <= worldArray[x, y].BlockArray.GetUpperBound(1); j++) {
    //					if(worldArray[x, y].BlockArray[i, j] != 0) {
    //						worldstring += x + ":" + y + ":" + i + ":" + j + ":" + worldArray[x, y].BlockArray[i, j] + "\n";
    //					}
    //				}
    //			}
    //		}
    //	}

    //	return worldstring;
    //}



    private void Update() {
        if(Input.GetMouseButton(0)) {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition) - new Vector3(0.5f, 0.5f);
            UpdateBlock(Mathf.RoundToInt(mousePos.x), Mathf.RoundToInt(mousePos.y), 0);
        }


        if(Input.GetMouseButton(1)) {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition) - new Vector3(0.5f, 0.5f);
            UpdateBlock(Mathf.RoundToInt(mousePos.x), Mathf.RoundToInt(mousePos.y), blockID);
        }


        if(Input.GetKeyDown(KeyCode.Alpha1)) {
            blockID = 1;
            Debug.Log("block set to 1");
        }

        if(Input.GetKeyDown(KeyCode.Alpha2)) {
            blockID = 2;
            Debug.Log("block set to 2");
        }
    }

    List<Coords> affectedLights = new List<Coords>();

    public void UpdateBlock(int worldPosX, int worldPosY, byte newBlockID) {
		int chunkPosX = worldPosX / WorldSettings.ChunkSizeX;
		int chunkPosY = worldPosY / WorldSettings.ChunkSizeY;
        int blockPosX = worldPosX % WorldSettings.ChunkSizeX;
        int blockPosY = worldPosY % WorldSettings.ChunkSizeY;

        worldArray[GetWorldIndex(chunkPosX, chunkPosY)].BlockArray[GetBlockIndex(blockPosX, blockPosY)] = newBlockID;
        worldArray[GetWorldIndex(chunkPosX, chunkPosY)].IsDirty = true;
        int chunkID = GetWorldIndex(chunkPosX, chunkPosY);

        affectedLights = new List<Coords>();

        GetAffectedLights(new Coords(chunkPosX, chunkPosY, blockPosX, blockPosY));

        spreadLight(chunkPosX, chunkPosY, blockPosX, blockPosY, (byte)(newBlockID == 0 ? 10 : 0));

        foreach(Coords coords in affectedLights) {
            byte light = getBlockLight(coords.wX, coords.wY, coords.cX, coords.cY);
            spreadLight(coords.wX, coords.wY, coords.cX, coords.cY, light);
        }

        UpdateDirtyChunks();
    }


    private void GetAffectedLights(Coords coords) {
        GetAffectedLights(coords.wX, coords.wY, coords.cX, coords.cY);
    }
    private void GetAffectedLights(int wX, int wY, int x, int y) {
        wX = (wX * WorldSettings.ChunkSizeX + x) - Mathf.Abs((int)(wX * WorldSettings.ChunkSizeX + x) & WorldSettings.ChunkSizeX - 1);
        wY = (wY * WorldSettings.ChunkSizeY + y) - Mathf.Abs((int)(wY * WorldSettings.ChunkSizeY + y) & WorldSettings.ChunkSizeY - 1);

        wX /= WorldSettings.ChunkSizeX;
        wY /= WorldSettings.ChunkSizeY;

        if(wX < 0 || wX >= WorldSettings.WorldSizeX)
            return;

        if(wY < 0 || wY >= WorldSettings.WorldSizeY)
            return;

        x = Utils.Mod(x, WorldSettings.ChunkSizeX);
        y = Utils.Mod(y, WorldSettings.ChunkSizeY);

        byte blockLight = getBlockLight(wX, wY, x, y);
        int worldIndex = GetWorldIndex(wX, wY);
        int blockIndex = GetBlockIndex(x, y);

        worldArray[worldIndex].IsDirty = true;

        // If light level is 0, just skip it
        if(worldArray[worldIndex].LightArray[blockIndex] == 0)
            return;

        // set current blocks light to 0
        worldArray[worldIndex].LightArray[blockIndex] = 0;

        for(int i = -1; i <= 1; i += 2) {
            Coords nCoords = new Coords(wX, wY, x + i, y);

            byte nLight = getBlockLight(nCoords.wX, nCoords.wY, nCoords.cX, nCoords.cY);

            // If light is the same or greater than blocklight, use it to recalculate the neighbours
            if(nLight >= blockLight) {
                affectedLights.Add(nCoords);
            } else if(nLight == blockLight - 2) {
                GetAffectedLights(nCoords);
            }
        }

        for(int i = -1; i <= 1; i += 2) {
            Coords nCoords = new Coords(wX, wY, x, y + i);

            byte nLight = getBlockLight(nCoords.wX, nCoords.wY, nCoords.cX, nCoords.cY);

            // If light is the same or greater than blocklight, use it to recalculate the neighbours
            if(nLight >= blockLight) {
                affectedLights.Add(nCoords);
            } else if(nLight == blockLight - 2) {
                GetAffectedLights(nCoords);
            }
        }
    }

	
	private ChunkMB getChunkFromPosition(Vector3 pos) {
		foreach(ChunkMB cmb in chunkMBList) {
			if(cmb.ChunkData.ChunkPosition.x == (int)pos.x && cmb.ChunkData.ChunkPosition.y == (int)pos.y) {
				return cmb;
			}
		}
		
		return null;
	}
	
	
	private void addUV(byte density, ref List<Vector2> uvs) {
		float tileSize = 1 / 8f;
        float x = density;
        float y = 8f;
        float offset = 0.002f;

        while (x >= 8) {
            x -= 8;
            y--;
        }

        Vector2 leftBottom		= new Vector2(Mathf.Abs(x * tileSize) + offset, tileSize * (y - 1f) + offset);
        Vector2 leftTop   		= new Vector2(Mathf.Abs(x * tileSize) + offset, tileSize * y - offset);
        Vector2 rightTop  		= new Vector2(Mathf.Abs((x + 1) * tileSize) - offset, tileSize * y - offset);
        Vector2 rightBottom 	= new Vector2(Mathf.Abs((x + 1) * tileSize) - offset, tileSize * (y - 1f) + offset);
                
        uvs.Add(leftBottom);
        uvs.Add(rightTop);
        uvs.Add(rightBottom);
        uvs.Add(leftTop);
    }


    private void ResetChunkLight(int chunkIndex) {
        for(int j = 0; j < worldArray[chunkIndex].BlockArray.Length; j++) {
            if(worldArray[chunkIndex].BlockArray[j] == 0) {
                worldArray[chunkIndex].LightArray[j] = 10;
            } else {
                worldArray[chunkIndex].LightArray[j] = 0;
            }
        }
    }
	
	
	private void fillSunlight() {
		for(int i = 0; i < worldArray.Length; i++) {
			for(int j = 0; j < worldArray[i].BlockArray.Length; j++) {
                if(worldArray[i].BlockArray[j] == 0) {
                    worldArray[i].LightArray[j] = 10;
                } else {
                    worldArray[i].LightArray[j] = 0;
                }
			}
        }

        for(int i = 0; i < worldArray.Length; i++) {
            int wX = (int)worldArray[i].ChunkPosition.x / WorldSettings.ChunkSizeX;
            int wY = (int)worldArray[i].ChunkPosition.y / WorldSettings.ChunkSizeY;

            for(int j = 0; j < worldArray[i].BlockArray.Length; j++) {
                if(worldArray[i].LightArray[j] == 10) {
                    int bX = j % WorldSettings.ChunkSizeX;
                    int bY = j / WorldSettings.ChunkSizeX;

                    spreadLight(wX, wY, bX, bY, 10);
                }
            }
        }
    }


    private void UpdateDirtyChunks() {
        foreach(Chunk chunk in worldArray) {
            if(chunk.IsDirty) {
                generateMesh(chunk);
            }
        }
    }
	
	
	// TODO: Fix the neighbour block check!
	private void spreadLight(int wX, int wY, int x, int y, byte light) {
        if(light < 2) {
            return;
        }

        wX = (wX * WorldSettings.ChunkSizeX + x) - Mathf.Abs((int)(wX * WorldSettings.ChunkSizeX + x) & WorldSettings.ChunkSizeX - 1);
        wY = (wY * WorldSettings.ChunkSizeY + y) - Mathf.Abs((int)(wY * WorldSettings.ChunkSizeY + y) & WorldSettings.ChunkSizeY - 1);

        wX /= WorldSettings.ChunkSizeX;
        wY /= WorldSettings.ChunkSizeY;

        if(wX < 0 || wX >= WorldSettings.WorldSizeX)
            return;

        if(wY < 0 || wY >= WorldSettings.WorldSizeY)
            return;

        x = Utils.Mod(x, WorldSettings.ChunkSizeX);
        y = Utils.Mod(y, WorldSettings.ChunkSizeY);

        if(light != 10 && light < worldArray[GetWorldIndex(wX, wY)].LightArray[GetBlockIndex(x, y)])
			return;

        worldArray[GetWorldIndex(wX, wY)].LightArray[GetBlockIndex(x, y)] = light;
        worldArray[GetWorldIndex(wX, wY)].IsDirty = true;

        light -= 2;

        spreadLight(wX, wY, x - 1, y, light);
        spreadLight(wX, wY, x + 1, y, light);
        spreadLight(wX, wY, x, y - 1, light);
        spreadLight(wX, wY, x, y + 1, light);
    }
	
	
	private byte getBlockLight(Chunk c, int x, int y) {
		return getBlockLight((int)c.ChunkPosition.x / WorldSettings.ChunkSizeX, (int)c.ChunkPosition.y / WorldSettings.ChunkSizeY, x, y);
	}
	private byte getBlockLight(int wX, int wY, int x, int y) {
		wX = (wX * WorldSettings.ChunkSizeX + x) - Mathf.Abs((int)(wX * WorldSettings.ChunkSizeX + x) & WorldSettings.ChunkSizeX - 1);
		wY = (wY * WorldSettings.ChunkSizeY + y) - Mathf.Abs((int)(wY * WorldSettings.ChunkSizeY + y) & WorldSettings.ChunkSizeY - 1);
		
		wX /= WorldSettings.ChunkSizeX;
		wY /= WorldSettings.ChunkSizeY;
		
		if(wX < 0 || wX >= WorldSettings.WorldSizeX)
			return 0;
		
		if(wY < 0 || wY >= WorldSettings.WorldSizeY)
			return 0;
		
		x = Utils.Mod(x, WorldSettings.ChunkSizeX);
		y = Utils.Mod(y, WorldSettings.ChunkSizeY);
		
		return worldArray[GetWorldIndex(wX, wY)].LightArray[GetBlockIndex(x, y)];
	}
	
	
	private void addFaces(ref List<int> triangles, ref List<Color> colors, int curVert, Color blockColor) {
		triangles.Add(curVert);
		triangles.Add(curVert + 1);
        triangles.Add(curVert + 2);

        triangles.Add(curVert);
		triangles.Add(curVert + 3);
        triangles.Add(curVert + 1);
		
		colors.Add(blockColor);
		colors.Add(blockColor);
		colors.Add(blockColor);
		colors.Add(blockColor);
	}


    private int GetTileID(Chunk c, int bX, int bY) {
        int wX = (int)c.ChunkPosition.x / WorldSettings.ChunkSizeX;
        int wY = (int)c.ChunkPosition.y / WorldSettings.ChunkSizeY;

        int index = 0;
        byte tileID = c.BlockArray[bX + bY * WorldSettings.ChunkSizeX];

        int top = hasNeighbourBlock(wX, wY, bX, bY + 1, tileID);
        int left = hasNeighbourBlock(wX, wY, bX - 1, bY, tileID);
        int right = hasNeighbourBlock(wX, wY, bX + 1, bY, tileID);
        int bottom = hasNeighbourBlock(wX, wY, bX, bY - 1, tileID);

        if(top + left + right + bottom == 0) {
            index = 256;
        } else {

            int topLeft = (top == 0 || left == 0) ? 0 : hasNeighbourBlock(wX, wY, bX - 1, bY + 1, tileID);
            int topRight = (top == 0 || right == 0) ? 0 : hasNeighbourBlock(wX, wY, bX + 1, bY + 1, tileID);
            int bottomLeft = (bottom == 0 || left == 0) ? 0 : hasNeighbourBlock(wX, wY, bX - 1, bY - 1, tileID);
            int bottomRight = (right == 0 || bottom == 0) ? 0 : hasNeighbourBlock(wX, wY, bX + 1, bY - 1, tileID);

            index = topLeft +
                2 * top +
                4 * topRight +
                8 * left +
                16 * right +
                32 * bottomLeft +
                64 * bottom +
                128 * bottomRight;
        }

        return tileIDMap.IndexOf(index);
    }
	
	
	private void generateMesh(Chunk c) {
		List<int>[] triangles	= new List<int>[3];
		List<Vector2> uvs 		= new List<Vector2>();
		List<Vector3> verts 	= new List<Vector3>();
		List<Color> colors 		= new List<Color>();

        for(int i = 0; i < triangles.Length; i++) {
            triangles[i] = new List<int>() { };
        }
		
		int curVert = 0;
		
		for(int x = 0; x < WorldSettings.ChunkSizeX; x++) {
			for(int y = 0; y < WorldSettings.ChunkSizeY; y++) {
                int blockIndex = GetBlockIndex(x, y);
                int tile = c.BlockArray[blockIndex];

                if(tile == 0)
					continue;
				
				verts.Add(new Vector3(x, y, 0));
				verts.Add(new Vector3(x + 1, y + 1, 0));
				verts.Add(new Vector3(x + 1, y, 0));
				verts.Add(new Vector3(x, y + 1, 0));

                int tileID = GetTileID(c, x, y);
                addUV((byte)tileID, ref uvs);

                triangles[tile - 1].Add(curVert);
                triangles[tile - 1].Add(curVert + 1);
                triangles[tile - 1].Add(curVert + 2);

                triangles[tile - 1].Add(curVert);
                triangles[tile - 1].Add(curVert + 3);
                triangles[tile - 1].Add(curVert + 1);

                float lightValue = (float)c.LightArray[blockIndex] / 10f;
				Color blockColor = new Color(lightValue, lightValue, lightValue);
				
				/*
				yellow   blue
				+-------+
				|       |
				|       |
				|       |
				+-------+
				red      green
				*/
				
				// Red Corner
				Color cornerColor = blockColor;
				lightValue = (float)getBlockLight(c, x - 1, y) / 10f;
				cornerColor += new Color(lightValue, lightValue, lightValue);
				lightValue = (float)getBlockLight(c, x - 1, y - 1) / 10f;
				cornerColor += new Color(lightValue, lightValue, lightValue);
				lightValue = (float)getBlockLight(c, x, y - 1) / 10f;
				cornerColor += new Color(lightValue, lightValue, lightValue);
				cornerColor /= 4f;
				
				colors.Add(cornerColor);
				
                // Blue Corner
				cornerColor = blockColor;
				lightValue = (float)getBlockLight(c, x + 1, y) / 10f;
				cornerColor += new Color(lightValue, lightValue, lightValue);
				lightValue = (float)getBlockLight(c, x + 1, y + 1) / 10f;
				cornerColor += new Color(lightValue, lightValue, lightValue);
				lightValue = (float)getBlockLight(c, x, y + 1) / 10f;
				cornerColor += new Color(lightValue, lightValue, lightValue);
				cornerColor /= 4f;
				
				colors.Add(cornerColor);
				
                // Green Corner
				cornerColor = blockColor;
				lightValue = (float)getBlockLight(c, x + 1, y) / 10f;
				cornerColor += new Color(lightValue, lightValue, lightValue);
				lightValue = (float)getBlockLight(c, x + 1, y - 1) / 10f;
				cornerColor += new Color(lightValue, lightValue, lightValue);
				lightValue = (float)getBlockLight(c, x, y - 1) / 10f;
				cornerColor += new Color(lightValue, lightValue, lightValue);
				cornerColor /= 4f;
				
				colors.Add(cornerColor);
				
                // Yellow Corner
				cornerColor = blockColor;
				lightValue = (float)getBlockLight(c, x - 1, y) / 10f;
				cornerColor += new Color(lightValue, lightValue, lightValue);
				lightValue = (float)getBlockLight(c, x - 1, y + 1) / 10f;
				cornerColor += new Color(lightValue, lightValue, lightValue);
				lightValue = (float)getBlockLight(c, x, y + 1) / 10f;
				cornerColor += new Color(lightValue, lightValue, lightValue);
				cornerColor /= 4f;
				
				colors.Add(cornerColor);
				
				curVert += 4;
				
				// Top face for collision
				//if(!hasNeighbourBlock(c, x, y + 1)) {
				//	verts.Add(new Vector3(x, y + 1, 0));
				//	verts.Add(new Vector3(x + 1, y + 1, 1));
				//	verts.Add(new Vector3(x + 1, y + 1, 0));
				//	verts.Add(new Vector3(x, y + 1, 1));
					
	   //             addUV(0, ref uvs);
					
				//	addFaces(ref triangles, ref colors, curVert, defaultBlockColor);
					
				//	curVert += 4;
				//}
				
				
				// Left face for collision
				//if(!hasNeighbourBlock(c, x - 1, y)) {
				//	verts.Add(new Vector3(x, y, 0));
				//	verts.Add(new Vector3(x, y + 1, 1));
				//	verts.Add(new Vector3(x, y + 1, 0));
				//	verts.Add(new Vector3(x, y, 1));
					
	   //             addUV(0, ref uvs);
	
	   //            	addFaces(ref triangles, ref colors, curVert, defaultBlockColor);
					
				//	curVert += 4;
				//}
				
				
				// Right face for collision
				//if(!hasNeighbourBlock(c, x + 1, y)) {
				//	verts.Add(new Vector3(x + 1, y, 0));
				//	verts.Add(new Vector3(x + 1, y + 1, 1));
				//	verts.Add(new Vector3(x + 1, y, 1));
				//	verts.Add(new Vector3(x + 1, y + 1, 0));
					
	   //             addUV(0, ref uvs);
	
	   //            	addFaces(ref triangles, ref colors, curVert, defaultBlockColor);
					
				//	curVert += 4;
				//}
				
				
				// Bottom face for collision
				//if(!hasNeighbourBlock(c, x, y - 1)) {
				//	verts.Add(new Vector3(x, y, 1));
				//	verts.Add(new Vector3(x + 1, y, 0));
				//	verts.Add(new Vector3(x + 1, y, 1));
				//	verts.Add(new Vector3(x, y, 0));
					
	   //             addUV(0, ref uvs);
	
	   //            	addFaces(ref triangles, ref colors, curVert, defaultBlockColor);
					
				//	curVert += 4;
				//}
			}
		}
		
		c.Triangles = triangles;
		c.Verts 	= verts;
		c.UVs 		= uvs;
		c.Colors 	= colors;
		
		ChunkMB cmb = getChunkFromPosition(c.ChunkPosition);

        c.IsDirty = false;
		
		if(cmb != null) {
			cmb.RegenerateMesh();
		}
	}
	
	
	private Coords getNeighbourBlock(int wX, int wY, int x, int y) {
		Coords co = new Coords();
		
		wX = (wX * WorldSettings.ChunkSizeX + x) - Mathf.Abs((int)(wX * WorldSettings.ChunkSizeX + x) & WorldSettings.ChunkSizeX - 1);
		wY = (wY * WorldSettings.ChunkSizeY + y) - Mathf.Abs((int)(wY * WorldSettings.ChunkSizeY + y) & WorldSettings.ChunkSizeY - 1);
		
		wX /= WorldSettings.ChunkSizeX;
		wY /= WorldSettings.ChunkSizeY;

		x = Utils.Mod(x, WorldSettings.ChunkSizeX);
		y = Utils.Mod(y, WorldSettings.ChunkSizeY);
		
		co.wX = wX;
		co.wY = wY;
		co.cX = x;
		co.cY = y;
		
		return co;
	}
	
	private int hasNeighbourBlock(Chunk c, int x, int y, byte tileID) {
		return hasNeighbourBlock((int)c.ChunkPosition.x / WorldSettings.ChunkSizeX, (int)c.ChunkPosition.y / WorldSettings.ChunkSizeY, x, y, tileID);
	}
	private int hasNeighbourBlock(int wX, int wY, int x, int y, byte tileID) {
		wX = (wX * WorldSettings.ChunkSizeX + x) - Mathf.Abs((int)(wX * WorldSettings.ChunkSizeX + x) & WorldSettings.ChunkSizeX - 1);
		wY = (wY * WorldSettings.ChunkSizeY + y) - Mathf.Abs((int)(wY * WorldSettings.ChunkSizeY + y) & WorldSettings.ChunkSizeY - 1);
		
		wX /= WorldSettings.ChunkSizeX;
		wY /= WorldSettings.ChunkSizeY;
		
		if(wX < 0 || wX >= WorldSettings.WorldSizeX)
			return 0;
		
		if(wY < 0 || wY >= WorldSettings.WorldSizeY)
			return 0;
		
		x = Utils.Mod(x, WorldSettings.ChunkSizeX);
		y = Utils.Mod(y, WorldSettings.ChunkSizeY);

        int newTileID = worldArray[GetWorldIndex(wX, wY)].BlockArray[GetBlockIndex(x, y)];

        if(newTileID == 0 || newTileID != tileID)
			return 0;
		
		return 1;
	}
}
