using UnityEngine;
using System.Collections;

public static class WorldSettings {
    public static int WorldSizeX = 10;
    public static int WorldSizeY = 10;
    public static int ChunkSizeX = 16;
    public static int ChunkSizeY = 16;


    public static int ChunkSizeTotal {
        get {
            return ChunkSizeX * ChunkSizeY;
        }
    }
}
