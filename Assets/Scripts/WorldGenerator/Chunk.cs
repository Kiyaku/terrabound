using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class Chunk {
	public Vector3 ChunkPosition;
	public byte[] BlockArray;
	public byte[] LightArray;
    public bool IsDirty = false;
	
	public List<int>[] Triangles		= new List<int>[3];
	public List<Vector2> UVs 		= new List<Vector2>();
	public List<Vector3> Verts 		= new List<Vector3>();
	public List<Color> Colors 		= new List<Color>();
	
	
	public Chunk() {
        for(int i = 0; i < Triangles.Length; i++) {
            Triangles[i] = new List<int>();
        }
		BlockArray = new byte[WorldSettings.ChunkSizeX * WorldSettings.ChunkSizeY];
		LightArray = new byte[WorldSettings.ChunkSizeX * WorldSettings.ChunkSizeY];
	}
}
