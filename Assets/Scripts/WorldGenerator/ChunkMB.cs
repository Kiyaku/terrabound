using UnityEngine;
using System.Collections;

public class ChunkMB : MonoBehaviour {
	public Chunk ChunkData;


    public void Setup(Chunk cd, Material defaultMat = null) {
        ChunkData = cd;
        transform.position = new Vector3(ChunkData.ChunkPosition.x, ChunkData.ChunkPosition.y, 0);

        gameObject.AddComponent<MeshFilter>();
        //gameObject.AddComponent<MeshRenderer>();
        gameObject.AddComponent<MeshCollider>();

        MeshFilter meshFilter = gameObject.GetComponent<MeshFilter>();

        Mesh newMesh = new Mesh();
        newMesh.subMeshCount = 3;
        meshFilter.mesh = newMesh;

        GetComponent<Renderer>().material = defaultMat;

        for(int i = 0; i < Generator.ins.Tileset.Length; i++) {
            GetComponent<MeshRenderer>().materials[i].mainTexture = Generator.ins.Tileset[i];
        }

        this.enabled = true;
	}
	
	
	public void RegenerateMesh() {
		Mesh subMesh = gameObject.GetComponent<MeshFilter>().mesh;
        MeshCollider meshCollider = gameObject.GetComponent<MeshCollider>();

        subMesh.Clear();
        subMesh.vertices        = ChunkData.Verts.ToArray();

        int counter = 0;
        subMesh.subMeshCount    = ChunkData.Triangles.Length;

        for(int i = 0; i < ChunkData.Triangles.Length; i++) {
           // Debug.Log(i + " | " + ChunkData.Triangles[i].Count + " | " + subMesh.vertices.Length);

            if(ChunkData.Triangles[i].Count > 0) {
                subMesh.SetTriangles(ChunkData.Triangles[i].ToArray(), i);
            } else if(subMesh.vertices.Length > 0) {
                subMesh.SetTriangles(new int[] { 0, 0, 0 }, i);
            }

            counter++;
        }

        subMesh.uv              = ChunkData.UVs.ToArray();
        subMesh.colors          = ChunkData.Colors.ToArray();

        subMesh.RecalculateNormals();
        subMesh.Optimize();
        gameObject.GetComponent<MeshFilter>().mesh = subMesh;

        meshCollider.sharedMesh = null;
        meshCollider.sharedMesh = subMesh;
	}
}
