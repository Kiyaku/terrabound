using UnityEngine;
using System.Collections;

public static class Utils {
	public static int Mod(int x, int m) {
	    return (x % m + m) % m;
	}
}
